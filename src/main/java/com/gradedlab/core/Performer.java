package com.gradedlab.core;

class Performer {
	protected final int id;
	
	Performer(int id) throws IllegalArgumentException{
		if(id < 0)
			throw new IllegalArgumentException("Id cannot be negative");
		this.id = id;
	}
	
    int getId() {
    	return id;
    }
	
	int perform() {
		
		System.out.println(this.id+ "-" + "performer");
		return 1;
	}

}
